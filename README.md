# web2epub
**Script python permettant de convertir une page web en un livre électronique au format epub**

## Introduction

La lecture des articles longs des sites web sur mon écran de PC étant peu agréable et fatigante j'ai rapidement essayé de trouver un moyen de transférer ces article sur ma liseuse en les transformant en fichiers epub. Quelques extensions pour Firefox existent : GrabMyBooks, EpubPress ou ePub Creator par exemple. J'ai successivement utilisé les 3 mais ces extensions présentent quelques limitations, en particulier dans la gestions des images de la page web qui sont récupérées, ou pas, d'une manière un peu aléatoire.

Ce script python permet de faire cette conversion avec une gestion améliorée des images et la création d'une couverture minimale.

## Prérequis

**python** et **pip** doivent être installés sur votre machine.

Les packages python suivants doivent être installés avec pip :  

- **pillow** : bibliothèque graphique,  
- **readabilipy** : interface python de la bibliothèque readability.js qui permet d'extraire la partie principale d'une page web (utilisée pour le mode lecture de Firefox),   
- **ebooklib** : bibliothèque pour la création d'ebooks au format epub.  

**Node.js** est nécessaire au fonctionnement de readability.js et doit être installé _avant_ **readabilipy**. Sur une machine Linux cette installation se fait avec la commande :

`sudo apt-get install nodejs`

L'installation des packages python se fait avec les commandes suivantes :

`pip install pillow`  
`pip install readabilipy`  
`pip install ebooklib`  

## Utilisation

Le script s'utilise en ligne de commande. Il suffit de le télécharger et de l'installer dans un répertoire de travail.

L'utilisation la plus simple est de passer l'URL de la page à convertir comme paramètre au script :

`python web2epub.py https://www.unsite.com/une-page-web/`

Cela créera, dans le répertoire où est le script un fichier nommé : "Article Unsite - Titre de la page web.epub". Le titre de l'ebook sera celui de la page web et le nom de l'auteur simplement repris du nom du site, dans le cas de cet exemple "Article Unsite" pour "https://www.unsite.com/".

Le nom de l'auteur peut être forcé avec l'option -a, par exemple : `-a "Prénom Nom"`.   
Le titre peut être forcé de la même façon avec l'option -t, par exemple : `-t "Le titre que vous voulez"`.

Plusieurs URL peuvent être passé au script. Dans ce cas les options -a et -t sont obligatoires. Le titre de chaque page web apparaîtra dans la table des matières.

On peut aussi mettre la, ou les, URL(s) dans un fichier, une URL par ligne, avec l'option -f, par exemple `-f nom_de_fichier`. Comme précédemment, si plusieurs URL sont fournies, les options -a et -t sont obligatoires.
