# Simple use

python ../web2epub.py https://framablog.org/2020/11/17/sy-retrouver-dans-le-framabazar-nous-parler-nous-soutenir-edition-2020/

# With an url list file

python ../web2epub.py -a "Cory Doctorow" -t "Détruire le capitalisme de surveillance" -f list.txt

