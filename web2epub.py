# coding: utf8

import sys, requests, re, io, uuid, argparse, os
from readabilipy import simple_json_from_html_string
from bs4 import BeautifulSoup
from PIL import Image
from ebooklib import epub

# Image max size to fit on ebook reader screen (taking into account margin)
# img_max_w = int(768 * 8 / 9)
# img_max_h = int(1024 * 104 / 122)
img_max_w = int(1072 * 81 / 90)
img_max_h = int(1448 * 96 / 120)

# Dictionary for image format
format = {
'jpg': 'JPEG',
'jpeg': 'JPEG',
'png': 'PNG',
'gif': 'GIF',
'webp': 'WEBP',
}

# Headers pour lire l'URL
headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}


# Check image URL and clean it
# Very simple check : search for image file extension in the string
def check_url(img_url, base_url, url):
    # Search for image file extension
    if re.match('^.*\.(?:png|jpg|jpeg|gif)', img_url, re.IGNORECASE) != None:
        # Remove everything after file extension (e.g. ?something)
        img_url = re.match('^.*\.(?:png|jpg|jpeg|gif)', img_url, re.IGNORECASE)[0]
        # Some site have images url starting with //, add https:
        if img_url[0:2] == '//':
            img_url = 'https:' + img_url
        # if the url not complete add site base url
        elif re.match('^http[s]{0,1}://', img_url) == None:
            if img_url[0:3] == '../':
                while img_url[0:3] == '../':
                    img_url = img_url[3:]
                    if url[-1:] == '/':
                        url = url[0:-1]
                    url_begin = url[0:url.rfind('/')+1]
                img_url = url_begin + img_url
            elif img_url[0] == '/' and base_url[-1:] == '/':
                img_url = base_url + img_url[1:]
            elif img_url[0] != '/' and base_url[-1:] != '/':
                img_url = base_url + '/' + img_url
            else:
                img_url = base_url + img_url
        return True, img_url
    # Some sites have no image URL extension but have a format information instead
    elif re.match('^.*format=jpg.*', img_url, re.IGNORECASE) != None:
        # if the url not complete add site base url
        if re.match('^http[s]{0,1}://', img_url) == None:
            if img_url[0] == '/' and base_url[-1:] == '/':
                img_url = base_url + img_url[1:]
            elif img_url[0] != '/' and base_url[-1:] != '/':
                img_url = base_url + '/' + img_url
            else:
                img_url = base_url + img_url
        return True, img_url
    # Some sites have images hosted by googleusercontent.com and have no extension in url
    elif re.match('.*googleusercontent.*', img_url, re.IGNORECASE) != None:
        return True, img_url
    # clubic.com has images without extension
    elif re.match('.*pic\.clubic\.com.*raw', img_url, re.IGNORECASE) != None:
        return True, img_url
    else:
        return False, ''

# Get image from tags <img>
def get_image(tag, base_url, url):
    url_ok = False
    # First check src attribute if it exists
    if tag.has_attr('src'):
        img_url = tag['src']
        url_ok, img_url = check_url(img_url, base_url, url)
    # If no url found or no src attribute
    if not url_ok:
        # Search in other attributes
        for attr in tag.attrs.keys():
            img_url = tag[attr]
            url_ok, img_url = check_url(img_url, base_url, url)
            if url_ok:
                break
    # Get image and resize it if needed
    if url_ok:
        if base_url[0:4] == 'http':
            try:
                r = requests.get(img_url)
                image_bytes = io.BytesIO(r.content)
                image = Image.open(image_bytes)
                # ext = img_url.split('.')[-1].lower()
                # Get extention from PILLOW instead of from URL (some URL have no extension)
                ext = image.format.lower()
            except:
                image = Image.new('RGB', (200, 100), color = 'white')
                ext = 'jpg'
        else:
            image = Image.open(img_url)
            ext = image.format.lower()
    else:
        image = Image.new('RGB', (200, 100), color = 'white')
        ext = 'jpg'
    if image.size[0] > img_max_w or image.size[1] > img_max_h:
        image.thumbnail((img_max_w, img_max_h), Image.ANTIALIAS)
    return image, ext

# HTML data modification to avoid some side effects of readabilipy
def modify_html(data_in):
    # Remove tags <section> and </section> to avoid content suppression
    # (used by framablog.org for footnotes)
    data_out = re.sub('<section.*?>', '', data_in)
    data_out = data_out.replace('</section>', '')
    # Remove links within <figcaption> : in some cases links within <figcaption>
    # trigger an image removal (don't find in which condition this happens)
    soup = BeautifulSoup(data_out, 'html.parser')
    for tag in soup.find_all('figcaption'):
        for link in tag.find_all('a'):
            link.replace_with(link.text)
    # <img> embedded in <picture> are not displayed
    # remove <picture> and replace it by the <img> inside
    for picture in soup.find_all('picture'):
        image = picture.find('img')
        picture.replace_with(image)
    # if <img> embeded in <span> remove <span>
    for span in soup.find_all('span'):
        image = span.find('img')
        if image != None:
            span.replace_with(image)
    # if <img> embeded in <div> with class="marty-cell-media" remove <div> (clubic.com)
    # It is "media" in class name which triggers the removal (with other things)
    for div in soup.find_all('div', {'class': 'marty-cell-media'}):
        image = div.find('img')
        if image != None:
            div.replace_with(image)
    # Remove div with class slice-inner__title (advertising - nouvelobs.com)
    for div in soup.find_all('div', {'class': 'slice-inner__title'}):
        div.extract()
    # Remove div with class paywall__btns (advertising - nouvelobs.com)
    for div in soup.find_all('div', {'class': 'paywall__btns'}):
        div.extract()
    # Readability removes paragraphs with links weight greater than 0.2
    # find these paragraph and remove links while keeping text
    for paragraph in soup.find_all('p'):
        texte = paragraph.get_text()
        if len(texte) != 0:
            link_weight = 0
            for link in paragraph.find_all('a'):
                link_weight += len(link.get_text())
            link_weight = link_weight/len(texte)
            if link_weight >= 0.2:
                paragraph.string = texte
    # Replace <iframe> with youtube video by an image with a link to the video
    for iframe in soup.find_all('iframe'):
        if iframe.has_attr('data-src'):
            iframe_target = iframe['data-src']
        elif iframe.has_attr('src'):
            iframe_target = iframe['src']
        else:
            iframe_target = ''
        if iframe_target != '' and 'www.youtube.com' in iframe_target:
            video_id = re.sub('https://www.youtube.com/embed/', '', iframe_target)
            video_id = re.sub('\?.*', '', video_id)
            # url of high quality thumbnail
            video_image = 'https://img.youtube.com/vi/' + video_id + '/maxresdefault.jpg'
            # link to youtube video
            video_link = 'https://www.youtube.com/watch?v=' + video_id
            # built image replacement with link to video
            image = soup.new_tag('img', alt="", src=video_image)
            link = soup.new_tag('a', href=video_link)
            link.append(image)
            iframe.replace_with(link)
    # Shift hx tags to avoid h1 removal (will be restored later)
    for h5 in soup.find_all('h5'):
        h5.name = 'h6'
    for h4 in soup.find_all('h4'):
        h4.name = 'h5'
    for h3 in soup.find_all('h3'):
        h3.name = 'h4'
    for h2 in soup.find_all('h2'):
        h2.name = 'h3'
    for h1 in soup.find_all('h1'):
        h1.name = 'h2'
    data_out = str(soup)

    return data_out

# Process URL
def process_url(url, n):
    # Inits
    try:
        base_url = re.match('http[s]{0,1}://.*?/', url)[0]
    except:
        print('URL : ' + url + ' incorrecte : ne commence pas par http:// ou https://')
        sys.exit()
    # Read URL
    try:
        r = requests.get(url, headers=headers)
    except:
        print('URL : ' + url + ' incorrecte. Erreur de connexion')
        sys.exit()
    # Check response and exit if error
    if r.status_code != requests.codes.ok:
        print('URL : ' + url + ' incorrecte. Code de retour : ' + str(r.status_code))
        sys.exit()
    # Fix possible encoding problem
    try:
        data = r.headers['Content-Type']
    except:
        data = ''
    # if no encoding in Content-Type try to find it in input
    if len(re.findall('charset="(.*)?;*', data)) == 0:
        if len(re.findall('charset="(.*)?"', r.text)) > 0:
            encoding = re.findall('charset="(.*)?"', r.text)[0].lower()
            r.encoding = encoding
    # Get content
    text = modify_html(r.text)
    article = simple_json_from_html_string(text, use_readability=True)
    title = article['title']
    author = 'Article ' + base_url[:-1].split('/')[-1].split('.')[-2].capitalize()
    soup = BeautifulSoup(article['content'], 'html.parser')
    # Restore hx tags
    for h2 in soup.find_all('h2'):
        h2.name = 'h1'
    for h3 in soup.find_all('h3'):
        h3.name = 'h2'
    for h4 in soup.find_all('h4'):
        h4.name = 'h3'
    for h5 in soup.find_all('h5'):
        h5.name = 'h4'
    for h6 in soup.find_all('h6'):
        h6.name = 'h5'
    # Search for images tags
    img_list = []
    for i, tag in enumerate(soup.find_all('img')):
        # Build attribute list
        list = []
        for attr in tag.attrs.keys():
            list.append(attr)
        # Get images
        image, ext = get_image(tag, base_url, url)
        image_file_name = 'IMG/image_' + str(n) + '_' + str(i) + '.' + ext
        tmp_tupple = (image, image_file_name, format[ext])
        img_list.append(tmp_tupple)
        # Remove all attributes and add src and alt attribute
        for attr in list:
            del tag[attr]
        tag['src'] = image_file_name
        tag['alt'] = ''
    content = soup.prettify("utf-8")
    return author, title, content, img_list

# Process file (HTML file on local disk)
def process_file(url, n):
    # Check if fiel exists
    if not os.path.isfile(url):
        print('Le fichier : ' + url + ' n\'existe pas')
        sys.exit()
    # Inits
    try:
        base_url = re.match('.*/', url)[0]
    except:
        base_url = ''
    # Read file
    file = open(url, 'r')
    content = file.read()
    file.close()
    # Get content
    text = modify_html(content)
    article = simple_json_from_html_string(text, use_readability=True)
    title = article['title']
    author = ''
    soup = BeautifulSoup(article['content'], 'html.parser')
    # Restore hx tags
    for h2 in soup.find_all('h2'):
        h2.name = 'h1'
    for h3 in soup.find_all('h3'):
        h3.name = 'h2'
    for h4 in soup.find_all('h4'):
        h4.name = 'h3'
    for h5 in soup.find_all('h5'):
        h5.name = 'h4'
    for h6 in soup.find_all('h6'):
        h6.name = 'h5'
    # Search for images tags
    img_list = []
    for i, tag in enumerate(soup.find_all('img')):
        # Build attribute list
        list = []
        for attr in tag.attrs.keys():
            list.append(attr)
        # Get images
        image, ext = get_image(tag, base_url, url)
        image_file_name = 'IMG/image_' + str(n) + '_' + str(i) + '.' + ext
        tmp_tupple = (image, image_file_name, format[ext])
        img_list.append(tmp_tupple)
        # Remove all attributes and add src and alt attribute
        for attr in list:
            del tag[attr]
        tag['src'] = image_file_name
        tag['alt'] = ''
    content = soup.prettify("utf-8")
    return author, title, content, img_list

# Main program
def main(argv):

    # Inits
    author = ''
    title = ''
    spine = []
    toc = ()

    # Parse command line
    description = 'Convertisseur de page web en fichier ebook au format epub.'
    description += ' Par défaut le titre de l\'ebook est celui de la page web'
    description += ' et le nom de l\'auteur est extrait de l\'URL du site'
    description += ' par exemple "Article Nom_site" pour le site "www.nom_site.fr".'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-a', '--author', help='remplacer le nom de l\'auteur par AUTHOR')
    parser.add_argument('-t', '--title', help='remplacer le titre par TITLE')
    help = 'fichier contenant une liste d\'URL à mettre dans l\'ebook.'
    help += ' Dans ce cas les paramètres -a AUTHOR et -t TITLE sont obligatoires,'
    help += ' sauf s\'il n\'y a qu\'une seule URL dans le fichier.'
    parser.add_argument('-f', '--file', help=help)
    help = 'URL de la page à convertir.'
    help += ' Si plusieurs URL sont présentes les paramètres -a AUTHOR et -t TITLE sont obligatoires.'
    parser.add_argument('url', help=help, nargs='*')
    args = parser.parse_args()

    # Decode command line arguments
    url_list = []
    if args.author != None:
        author = args.author
    if args.title != None:
        title = args.title
    if args.file != None:
        with open(args.file, 'r') as f:
            url_list = f.read().splitlines()
        if len(url_list) == 0:
            print('Erreur : le fichier d\'URLs est vide\n')
            parser.print_help()
            sys.exit()
        elif len(url_list) > 1:
            if args.author == None or args.title == None:
                print('options -a et -t obligatoires avec l\'option -f\n')
                parser.print_help()
                sys.exit()
    else:
        if len(args.url) == 0:
            print('Erreur : paramètre URL manquant\n')
            parser.print_help()
            sys.exit()
        elif len(args.url) == 1:
            url_list.append(args.url[0])
        else:
            if args.author == None or args.title == None:
                print('options -a et -t obligatoires avec plusieurs URLs\n')
                parser.print_help()
                sys.exit()
            else:
                url_list = args.url

    # Create ebook
    book = epub.EpubBook()
    # metadata
    # book.set_identifier(uuid.uuid1())
    book.set_identifier(uuid.uuid1().hex)
    book.set_language('fr')
    # add default NCX and Nav file
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    # Clean URL list : remove empty strings if any
    while '' in url_list:
        url_list.remove('')
    # Process URLs and add its content to ebook
    for n, url in enumerate(url_list):
        if url[0:4] == 'http':
            a, t, content, img_list = process_url(url, n)
        else:
            a, t, content, img_list = process_file(url, n)
        if len(url_list) == 1:
            if title == '':
                title = t
            if author == '':
                author = a
        # Add url content and images to ebook
        file_name = 'article_' + str(n) + '.xhtml'
        chapter = epub.EpubHtml(title=t, file_name=file_name, lang='fr')
        chapter.content = content
        book.add_item(chapter)
        # Add images
        for img in img_list:
            b = io.BytesIO()
            img[0].save(b, img[2])
            media_type = 'image/' + img[2].lower()
            img1 = epub.EpubItem(file_name=img[1], media_type=media_type, content=b.getvalue())
            book.add_item(img1)
        spine.append(chapter)
        toc += (chapter, )

    # If several URLs add nav to spine
    if len(url_list) > 1:
        spine = ['nav'] + spine

    # Make simple cover page
    title_page = epub.EpubHtml(title='Couverture', file_name='cover.xhtml', lang='fr')
    cover = '<center><h1><br />' + title + '<br /><br /><br />' + author + '</h1></center>'
    title_page.content=cover.encode('utf8')
    book.add_item(title_page)
    spine = [title_page] + spine
    toc = (title_page, ) + toc

    # Author and title
    book.add_author(author)
    book.set_title(title)

    # Add table of content and spine
    book.toc = (toc)
    book.spine = spine

    # Save epub
    file_name = author + ' - ' + title + '.epub'
    file_name = file_name.replace('/', '-')         # Remove / in file name if any
    epub.write_epub(file_name, book, {})


if __name__ == '__main__':
    main(sys.argv[1:])
